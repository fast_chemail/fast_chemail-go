// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fastchemail

import (
	"bytes"
	"strconv"
)

const maxASCII = 127

func isLetter(char rune) bool {
	char &^= 'a' - 'A'
	return 'A' <= char && char <= 'Z'
}

func isDigit(char rune) bool { return '0' <= char && char <= '9' }

// checkASCIIPrintable reports an error wheter the string has a non-ASCII
// character or any ASCII control character.
func checkASCIIPrintable(str string) error {
	buf := bytes.NewReader([]byte(str))

	for i := 1; ; i++ {
		_rune, n, err := buf.ReadRune()
		if err != nil { // == io.EOF
			break
		}

		codeRune := int(_rune)
		if n > 1 || codeRune > maxASCII {
			return NonASCIIError(_rune)
		}
		if 0 <= codeRune && codeRune <= 31 || codeRune == 127 {
			return ControlCharError(i)
		}
	}
	return nil
}

// == Errors
//

// NonASCIIError reports error NoAscii.
type NonASCIIError rune

func (e NonASCIIError) Error() string {
	return "contain non US-ASCII character (" + string(e) + ")"
}

// ControlCharError reports error ControlChar.
type ControlCharError int

func (e ControlCharError) Error() string {
	return "contain ASCII control character at position " + strconv.Itoa(int(e))
}
