// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fastchemail

import "testing"

func Test_isLetter(t *testing.T) {
	tableTests := []struct {
		letter rune
		expect bool
	}{
		{'a', true},
		{'j', true},
		{'z', true},
		{'A', true},
		{'J', true},
		{'Z', true},
		{'0', false},
	}

	for i, tt := range tableTests {
		if isLetter(tt.letter) != tt.expect {
			t.Errorf("[%d] `%v` got `%v`", i, tt.letter, tt.expect)
		}
	}
}

func Test_isDigit(t *testing.T) {
	tableTests := []struct {
		letter rune
		expect bool
	}{
		{'0', true},
		{'5', true},
		{'9', true},
		{'a', false},
	}

	for i, tt := range tableTests {
		if isDigit(tt.letter) != tt.expect {
			t.Errorf("[%d] `%v` got `%v`", i, tt.letter, tt.expect)
		}
	}
}

func Test_checkASCIIPrintable(t *testing.T) {
	inputOk := "foo"
	if err := checkASCIIPrintable(inputOk); err != nil {
		t.Errorf("`%v` got error `%v`", inputOk, err)
	}

	for _, inputErr := range []string{"foó", "foo\tbar"} {
		if err := checkASCIIPrintable(inputErr); err == nil {
			t.Errorf("`%v` want error", inputErr)
		}
	}
}
