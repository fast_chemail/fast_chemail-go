// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fastchemail

import (
	"fmt"
	"strings"
	"testing"
)

var tableOk = [...]string{
	"!#$%&'*+-/=?^_`{|}~@example.com",
	"user+mailbox@example.com",
	"customer/department=shipping@example.com",
	"$A12345@example.com",
	"!def!xyz%abc@example.com",
	"_somename@example.com",
	"a@example.com",
	"a@x.y",
	"abc.def@example.com",
	"abc-def@example.com",
	"123@example.com",
	"xn--abc@example.com",
	"abc@x.y.z",
	"abc@xyz-example.com",
	"abc@c--n.com",
	"abc@xn--hxajbheg2az3al.xn--jxalpdlp",
	"abc@1example.com",
	"abc@x.123",
}

var tableError = [...]struct {
	email string
	err   error
}{
	{"@", ErrNoLocalPart},
	{"@example.com", ErrNoLocalPart},
	{"abc@", ErrNoDomainPart},
	{"abc", ErrNoSignAt},
	{"abc@def@example.com", ErrTooAt},
	{".abc@example.com", ErrLocalStartPeriod},
	{"abc.@example.com", ErrLocalEndPeriod},
	{"abc@.example.com", ErrDomainStartPeriod},
	{"abc@example.com.", ErrDomainEndPeriod},
	{"ab..cd@example.com", ErrConsecutivePeriod},
	{"abc@example..com", ErrConsecutivePeriod},
	{"a@example", ErrNoPeriodDomain},
	{`ab\c@example.com`, WrongCharLocalError('\\')},
	{`ab"c"def@example.com`, WrongCharLocalError('"')},
	{"abc def@example.com", WrongCharLocalError(' ')},
	{"(comment)abc@example.com", WrongCharLocalError('(')},
	{"abc@[255.255.255.255]", WrongCharDomainError('[')},
	{"abc@(example.com", WrongCharDomainError('(')},
	{"abc@x.y_y.z", WrongCharDomainError('_')},
	{"abc@-example.com", WrongStartLabelError('-')},
	{"abc@example-.com", WrongEndLabelError('-')},
	{"abc@x.-y.z", WrongStartLabelError('-')},
	{"abc@x.y-.z", WrongEndLabelError('-')},
	{"abcd€f@example.com", NonASCIIError('€')},
	{"abc@exámple.com", NonASCIIError('á')},
	{"a\tbc@example.com", ControlCharError(2)},
	{"abc@\texample.com", ControlCharError(5)},
}

func Test_ParseEmail(t *testing.T) {
	for i, tt := range tableOk {
		if err := ParseEmail(tt); err != nil {
			t.Errorf("[%d] got error `%v`", i, err)
		}
	}
	for i, tt := range tableError {
		if err := ParseEmail(tt.email); err != tt.err {
			t.Errorf("[%d] (%v) got `%v`, want `%v`", i, tt.email, err, tt.err)
		}
	}
}

func Test_IsValidEmail(t *testing.T) {
	if !IsValidEmail(tableOk[0]) {
		t.Errorf("[%d] want `true`", 0)
	}

	if IsValidEmail(tableError[0].email) {
		t.Errorf("[%d] want `false`", 0)
	}
}

func Test_length(t *testing.T) {
	localPart := strings.Repeat("a", maxLocalPart)

	label := strings.Repeat("x", maxLabel) + "."
	allLabels := strings.Repeat(label, 3)
	lastLabel := strings.Repeat("y", maxDomainPart-len(allLabels))

	inputOk := fmt.Sprintf("%s@%s%s", localPart, allLabels, lastLabel)
	err := ParseEmail(inputOk)
	if err != nil {
		t.Errorf("`%v` got error `%v`", inputOk, err)
	}

	// == Errors

	inputErr := fmt.Sprintf("a%s@%s%s", localPart, allLabels, lastLabel)
	if err = ParseEmail(inputErr); err != ErrLocalTooLong {
		t.Errorf("`%v` want error `ErrLocalTooLong`", inputErr)
	}

	inputErr = fmt.Sprintf("%s@%s%sz", localPart, allLabels, lastLabel)
	if err = ParseEmail(inputErr); err != ErrDomainTooLong {
		t.Errorf("`%v` want error `ErrDomainTooLong`", inputErr)
	}

	inputErr = fmt.Sprintf("%s@%sz%s", localPart, label, lastLabel)
	if err = ParseEmail(inputErr); err != ErrLabelTooLong {
		t.Errorf("`%v` want error `ErrLabelTooLong`", inputErr)
	}
}
