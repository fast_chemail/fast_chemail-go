// Copyright 2016  Jonas Me
// See the 'AUTHORS' file at the top-level directory for a full list of authors.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

package fastchemail

import (
	"errors"
	"strings"
)

// https://tools.ietf.org/html/rfc5321#section-4.5.3.1
//
// The maximum total length of a user name or other local-part is 64 octets.
// The maximum total length of a domain name or number is 255 octets.
const (
	maxLocalPart  int = 64
	maxDomainPart int = 255
	maxLabel      int = 63
)

// IsValidEmail checks wheter an email address is valid.
func IsValidEmail(address string) bool {
	if err := ParseEmail(address); err != nil {
		return false
	}
	return true
}

// ParseEmail scans an email address to check wheter it is correct.
func ParseEmail(address string) error {
	if address[0] == '@' {
		return ErrNoLocalPart
	}
	if address[len(address)-1] == '@' {
		return ErrNoDomainPart
	}

	// https://tools.ietf.org/html/rfc5321#section-4.1.2
	//
	// Systems MUST NOT define mailboxes in such a way as to require the use in
	// SMTP of non-ASCII characters (octets with the high order bit set to one)
	// or ASCII "control characters" (decimal value 0-31 and 127).
	if err := checkASCIIPrintable(address); err != nil {
		return err
	}

	parts := strings.Split(address, "@")
	if len(parts) != 2 {
		if len(parts) > 2 {
			return ErrTooAt
		}
		return ErrNoSignAt
	}

	// == Local part

	// https://tools.ietf.org/html/rfc3696#section-3
	//
	// Period (".") may also appear, but may not be used to start or end
	// the local part, nor may two or more consecutive periods appear.

	if len(parts[0]) > maxLocalPart {
		return ErrLocalTooLong
	}
	if parts[0][0] == '.' {
		return ErrLocalStartPeriod
	}
	if parts[0][len(parts[0])-1] == '.' {
		return ErrLocalEndPeriod
	}

	lastPeriod := false
	for _, ch := range parts[0] {
		if isLetter(ch) || isDigit(ch) {
			if lastPeriod {
				lastPeriod = false
			}
			continue
		}

		switch ch {
		case '.':
			if lastPeriod {
				return ErrConsecutivePeriod
			}
			lastPeriod = true
		case '!', '#', '$', '%', '&', '\'', '*', '+', '-', '/', '=', '?', '^',
			'_', '`', '{', '|', '}', '~':
			// atom :: https://tools.ietf.org/html/rfc5322#section-3.2.3
			if lastPeriod {
				lastPeriod = false
			}
		default:
			return WrongCharLocalError(ch)
		}
	}

	// == Domain part

	// https://tools.ietf.org/html/rfc5321#section-4.1.2
	//
	// characters outside the set of alphabetic characters, digits, and hyphen
	// MUST NOT appear in domain name labels for SMTP clients or servers.  In
	// particular, the underscore character is not permitted.

	// https://tools.ietf.org/html/rfc1034#section-3.5
	//
	// The labels must follow the rules for ARPANET host names.  They must start
	// with a letter, end with a letter or digit, and have as interior
	// characters only letters, digits, and hyphen.  There are also some
	// restrictions on the length.  Labels must be 63 characters or less.

	// https://tools.ietf.org/html/rfc3696#section-2
	//
	// It is likely that the better strategy has now become to make the "at
	// least one period" test, to verify LDH conformance (including verification
	// that the apparent TLD name is not all-numeric), and then to use the DNS
	// to determine domain name validity, rather than trying to maintain a local
	// list of valid TLD names.

	if len(parts[1]) > maxDomainPart {
		return ErrDomainTooLong
	}
	if parts[1][0] == '.' {
		return ErrDomainStartPeriod
	}
	if parts[1][len(parts[1])-1] == '.' {
		return ErrDomainEndPeriod
	}

	labels := strings.Split(parts[1], ".")
	if len(labels) == 1 {
		return ErrNoPeriodDomain
	}

	// label = let-dig [ [ ldh-str ] let-dig ]
	// limited to a length of 63 characters by RFC 1034 section 3.5
	//
	// <let-dig> ::= <letter> | <digit>
	// <ldh-str> ::= <let-dig-hyp> | <let-dig-hyp> <ldh-str>
	// <let-dig-hyp> ::= <let-dig> | "-"
	for _, label := range labels {
		if len(label) == 0 {
			return ErrConsecutivePeriod
		}
		if len(label) > maxLabel {
			return ErrLabelTooLong
		}

		for _, ch := range label {
			if isLetter(ch) || isDigit(ch) || ch == '-' {
				continue
			}
			return WrongCharDomainError(ch)
		}

		firstChar := rune(label[0])
		if !isLetter(firstChar) && !isDigit(firstChar) {
			return WrongStartLabelError(label[0])
		}
		lastChar := rune(label[len(label)-1])
		if !isLetter(lastChar) && !isDigit(lastChar) {
			return WrongEndLabelError(lastChar)
		}
	}

	return nil
}

// == Errors
//

// Errors
var (
	ErrNoLocalPart       = errors.New("no local part")
	ErrNoDomainPart      = errors.New("no domain part")
	ErrNoSignAt          = errors.New("no at sign (@)")
	ErrTooAt             = errors.New("wrong number of at sign (@)")
	ErrLocalTooLong      = errors.New("the local part has more than 64 characters")
	ErrDomainTooLong     = errors.New("the domain part has more than 255 characters")
	ErrLabelTooLong      = errors.New("a domain label has more than 63 characters")
	ErrLocalStartPeriod  = errors.New("the local part starts with a period")
	ErrLocalEndPeriod    = errors.New("the local part ends with a period")
	ErrDomainStartPeriod = errors.New("the domain part starts with a period")
	ErrDomainEndPeriod   = errors.New("the domain part ends with a period")
	ErrConsecutivePeriod = errors.New("appear two or more consecutive periods")
	ErrNoPeriodDomain    = errors.New("no period at domain part")
)

// WrongCharLocalError reports error WrongCharLocal.
type WrongCharLocalError rune

func (e WrongCharLocalError) Error() string {
	return "character not valid in local part (" + string(e) + ")"
}

// WrongCharDomainError reports error WrongCharDomain.
type WrongCharDomainError rune

func (e WrongCharDomainError) Error() string {
	return "character not valid in domain part (" + string(e) + ")"
}

// WrongStartLabelError reports error WrongStartLabel.
type WrongStartLabelError rune

func (e WrongStartLabelError) Error() string {
	return "character not valid at start of domain label (" + string(e) + ")"
}

// WrongEndLabelError reports error WrongEndLabel.
type WrongEndLabelError rune

func (e WrongEndLabelError) Error() string {
	return "character not valid at end of domain label (" + string(e) + ")"
}
